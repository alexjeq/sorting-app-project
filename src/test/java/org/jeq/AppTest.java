package org.jeq;

import java.io.ByteArrayOutputStream;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

/**
 * Unit test for simple App.
 */

public class AppTest {

    public static void test(String[] actual, String[] expected) throws Exception {
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));
        App.main(actual);
        Assertions.assertEquals(Arrays.toString(expected), outputStreamCaptor.toString().trim());
    }

    /**
     * Rigorous Test :-)
     */

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase() throws Exception {
        App.main(null);
    }

    @Test
    public void testMoreThanTenArgsCase() {
        String[] actualArray = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "22"};
        try {
            App.main(actualArray);
        } catch (Exception e) {
            Assertions.assertEquals(Exception.class, e.getClass());
            Assertions.assertEquals("Error: More than 10 arguments!", e.getMessage());
        }
    }

    @Test
    public void testIllegalArgsCase() {
        String[] actualArray = new String[]{"1", "aa", "3", "4"};
        try {
            App.main(actualArray);
        } catch (Exception e) {
            Assertions.assertEquals(IllegalArgumentException.class, e.getClass());
            Assertions.assertEquals("Error: Not all arguments are numbers!", e.getMessage());
        }
    }

    @Test
    public void testEmptyCase() throws Exception {
        test(new String[]{}, new String[]{});
    }

    @Test
    public void testSingleElementArrayCase() throws Exception {
        test(new String[]{"1"}, new String[]{"1"});
    }

    @Test
    public void testSortedArraysCase() throws Exception {
        test(new String[]{"1", "2", "3"}, new String[]{"1", "2", "3"});
    }


    @ParameterizedTest
    @MethodSource
    public void testOtherCases(String[] actual, String[] expected) throws Exception {
        test(actual, expected);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testOtherCases() {
        return Arrays.asList(new Object[][]{
                {new String[]{"5", "2"}, new String[]{"2", "5"}},
                {new String[]{"5", "4", "2"}, new String[]{"2", "4", "5"}},
                {new String[]{"1", "5", "3", "2"}, new String[]{"1", "2", "3", "5"}},
                {new String[]{"1", "5", "3", "2", "7"}, new String[]{"1", "2", "3", "5", "7"}},
                {new String[]{"1", "5", "3", "2", "7", "4"}, new String[]{"1", "2", "3", "4", "5", "7"}},
                {new String[]{"1", "5", "3", "2", "7", "4", "8"}, new String[]{"1", "2", "3", "4", "5", "7", "8"}},
                {new String[]{"1", "5", "3", "2", "7", "4", "8", "6"}, new String[]{"1", "2", "3", "4", "5", "6", "7", "8"}},
                {new String[]{"1", "5", "3", "2", "7", "4", "8", "6", "9"}, new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9"}},
                {new String[]{"1", "5", "3", "2", "7", "4", "8", "6", "9", "10"}, new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}},

        });
    }
}
