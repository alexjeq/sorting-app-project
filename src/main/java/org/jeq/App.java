package org.jeq;

import java.util.Arrays;

public class App {
    public static void main(String[] args) throws Exception {
        if (args == null) {
            throw new IllegalArgumentException();
        } else if(args.length > 10){
            throw new Exception("Error: More than 10 arguments!");
        } else{
            int[] arrayToSort = new int[args.length];
            for (int i = 0; i < args.length; i++) {
                try {
                    arrayToSort[i] = Integer.parseInt(args[i]);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Error: Not all arguments are numbers!");
                }
            }
            Arrays.sort(arrayToSort);
            System.out.println(Arrays.toString(arrayToSort));
        }
    }
}
